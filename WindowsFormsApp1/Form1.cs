﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;using ClassLibrary1;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {

                double a = Convert.ToDouble(A.Text);
                double b = Convert.ToDouble(B.Text);
                double c = Convert.ToDouble(C.Text);

                double d = Class1.D(a, b, c, out d);
                double x1, x2;

                if (d < 0)
                {
                    MessageBox.Show("Solution does not exist");
                    A.Text = "";
                    A.Clear();
                    B.Text = "";
                    B.Clear();
                    C.Text = "";
                    C.Clear();
                }


                if (d == 0)
                {
                    D.Text = Convert.ToString(d);
                    x1 = Class1.X1(a, b, d, out x1);
                    X1.Text = Convert.ToString(x1);
                    X2.Visible = false;
                    labelX2.Visible = false;
                }


                if (d > 0)
                {
                    D.Text = Convert.ToString(d);
                    x1 = Class1.X1(a, b, d, out x1);
                    X1.Text = Convert.ToString(x1);
                    x2 = Class1.X2(a, b, d, out x2);
                    X2.Text = Convert.ToString(x2);

                    X1.Visible = true;
                    X2.Visible = true;
                    labelX1.Visible = true;
                    labelX2.Visible = true;

                }


                if (a == 0)
                {
                    MessageBox.Show("Solution does not exist");
                    A.Text = "";
                    A.Clear();
                    
                }


                if (b == 0)
                {
                    if (c < 0)
                    {
                        x1 = -Math.Sqrt(c);
                        X1.Text = Convert.ToString(x1);
                        x2 = Math.Sqrt(c);
                        X2.Text = Convert.ToString(x2);
                        X1.Visible = true;
                        X2.Visible = true;
                        labelX1.Visible = true;
                        labelX2.Visible = true;
                    }
                    if (c > 0) { MessageBox.Show("Solution does not exist"); A.Text = "";
                        A.Clear();
                        B.Text = "";
                        B.Clear();
                        C.Text = "";
                        C.Clear();
                    }
                }
            }
            catch(Exception ex)
            {
                
                MessageBox.Show("Помилка введення значення !", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                A.Text = "";
                A.Clear();
                B.Text = "";
                B.Clear();
                C.Text = "";
                C.Clear();
            }
        }

    }

}
